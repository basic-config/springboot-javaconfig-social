<%--
  Created by IntelliJ IDEA.
  User: ohs
  Date: 2017-01-13
  Time: 오후 3:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>회원가입</title>
</head>
<body>
    <form action="/register" method="post">
        <div>
            <div>firstName : <input type="text" id="firstName" name="firstName" value="${user.firstName}"></div>
            <div>LastName : <input type="text" id="lastName" name="lastName" value="${user.lastName}"></div>
            <div>userMail : <input type="text" id="email" name="email" value="${user.email}"></div>
            password는 social이 아닌경우에만 보이도록 한다.
            <div>userpassword : <input type="password" id="password" name="password"></div>
            <button type="submit">가입하기</button>
            <div>signInProvider : <input type="text" id="signInProvider" name="signInProvider" value="${user.signInProvider}"></div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </div>
    </form>
</body>
</html>
