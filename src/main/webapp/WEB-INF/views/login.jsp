<%--
  Created by IntelliJ IDEA.
  User: ohs
  Date: 2017-01-12
  Time: 오후 3:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>social Login</title>
</head>
<body>
    <form action="/login" method="post">
    <div>
        <div>사용자 로그인</div>
        <div>
            <input type="text" name="username" placeholder="User Name" />
        </div>
        <div>
            <input type="password" name="password" placeholder="Password" />
        </div>
        <div>
            <input type='checkbox' name="remember-me"/>Remember Me? <br/>
        </div>
        <div>
            <input type="submit" value="Sign In" class="button red small" />
        </div>
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
    </div>
        <div>
            <a href="${pageContext.request.contextPath}/register">회원가입</a>
        </div>
    <div>
        <a href="${pageContext.request.contextPath}/auth/facebook">facebook</a>
    </div>
    </form>
    <form action="/auth/google" method="post">
        <div>
            <input type="hidden" name="scope" value="https://www.googleapis.com/auth/plus.login" />
            <div>
                <input type="submit" value="google" />
            </div>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </div>
    </form>
</body>
</html>
