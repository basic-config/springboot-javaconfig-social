package net.suby.project.user.service.impl;

import java.util.Date;
import net.suby.project.login.dao.jpa.UserJpaRepository;
import net.suby.project.login.vo.UserAccounts;
import net.suby.project.user.service.UserService;
import net.suby.project.user.vo.RegistrationForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 * Created by ohs on 2017-01-13.
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Override
    public UserAccounts registerNewUserAccount(RegistrationForm userAccountData) throws Exception {
        if (emailExist(userAccountData.getEmail())) {
            throw new Exception("The email address: " + userAccountData.getEmail() + " is already in use.");
        }
        //String encodedPassword = encodePassword(userAccountData);
        UserAccounts userAccounts = new UserAccounts();
        userAccounts.setEmail(userAccountData.getEmail());
        userAccounts.setFirstName(userAccountData.getFirstName());
        userAccounts.setLastName(userAccountData.getLastName());
        userAccounts.setPassword(userAccountData.getPassword());
        userAccounts.setVersion(1L);
        userAccounts.setRole("ROLE_USER");

        Date dt = new Date();
        java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String currentDate = sdf.format(dt);
        userAccounts.setCreationTime(currentDate);
        userAccounts.setModificationTime(currentDate);
        if (userAccountData.isSocialSignIn()) {
            userAccounts.setSignInProvider(userAccountData.getSignInProvider());
        }

        return userJpaRepository.save(userAccounts);
    }

    private boolean emailExist(String email) {
        UserAccounts user = userJpaRepository.findByEmail(email);
        if (user != null) {
            return true;
        }
        return false;
    }

    private String encodePassword(RegistrationForm dto) {
        String encodedPassword = null;

        if (dto.isNormalRegistration()) {
            encodedPassword = passwordEncoder.encode(dto.getPassword());
        }

        return encodedPassword;
    }
}
