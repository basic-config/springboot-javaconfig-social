package net.suby.project.user.service;

import net.suby.project.login.vo.UserAccounts;
import net.suby.project.user.vo.RegistrationForm;

/**
 * Created by ohs on 2017-01-13.
 */
public interface UserService {
    public UserAccounts registerNewUserAccount(RegistrationForm userAccountData) throws Exception;
}
