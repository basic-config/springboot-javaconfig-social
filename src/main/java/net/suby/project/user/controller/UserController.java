package net.suby.project.user.controller;

import javax.validation.Valid;
import net.suby.project.login.vo.UserAccounts;
import net.suby.project.user.service.UserService;
import net.suby.project.user.vo.RegistrationForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionKey;
import org.springframework.social.connect.UserProfile;
import org.springframework.social.connect.web.ProviderSignInUtils;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;

/**
 * Created by ohs on 2017-01-13.
 */
@Controller
public class UserController {
    private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private ProviderSignInUtils providerSignInUtils;

    @RequestMapping(value = "/signup", method = RequestMethod.GET)  // facebook 기본 url
    public String signup() {
        return "redirect:/register";
    }

    @RequestMapping(value = "/signin", method = RequestMethod.GET)  // google 기본 url
    public String signin() {
        return "redirect:/register";
    }

    /**
     * social로그인이 되어 있다면 그 정보를 가지고 회원가입 페이지로 넘어간다.
     * @param request
     * @param model
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public String showRegistrationForm(WebRequest request, Model model) {
        Connection<?> connection = providerSignInUtils.getConnectionFromSession(request);
        RegistrationForm registrationForm = createRegistrationDTO(connection);
        model.addAttribute("user", registrationForm);

        return "signup";
    }

    private RegistrationForm createRegistrationDTO(Connection<?> connection) {
        RegistrationForm dto = new RegistrationForm();

        if (connection != null) {
            ConnectionKey providerKey = connection.getKey();
            dto.setSignInProvider(providerKey.getProviderId().toUpperCase());

            if(dto.getSignInProvider().equals("FACEBOOK")){
                Facebook facebook = (Facebook) connection.getApi();
                String [] fields = { "id", "email",  "first_name", "last_name" };
                User userProfile = facebook.fetchObject("me", User.class, fields);
                dto.setEmail(userProfile.getEmail());
                dto.setFirstName(userProfile.getFirstName());
                dto.setLastName(userProfile.getLastName());
            } else {
                UserProfile socialMediaProfile = connection.fetchUserProfile();
                dto.setEmail(socialMediaProfile.getEmail());
                dto.setFirstName(socialMediaProfile.getFirstName());
                dto.setLastName(socialMediaProfile.getLastName());
            }
        }

        return dto;
    }

    @RequestMapping(value ="/register", method = RequestMethod.POST)
    public String registerUserAccount(@Valid @ModelAttribute("user") RegistrationForm userAccountData,
            BindingResult result,
            WebRequest request) {

        if (result.hasErrors()) {
            LOGGER.debug("Validation errors found. Rendering form view.");
            return "/signup";
        }
        UserAccounts registered = createUserAccount(userAccountData, result);       // 사용자 계정 추가

        providerSignInUtils.doPostSignUp(registered.getEmail(), request);           // social정보 추가(UserConnection)

        return "redirect:/";
    }

    private UserAccounts createUserAccount(RegistrationForm userAccountData, BindingResult result) {
        UserAccounts registered = null;
        try {
            registered = userService.registerNewUserAccount(userAccountData);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return registered;
    }

    @RequestMapping(value = "/user/userView", method = RequestMethod.GET)
    public String userView(WebRequest request, Model model) {
        return "/user/userView";
    }
}
