package net.suby.project.config.security;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import net.suby.project.login.dao.jpa.UserJpaRepository;
import net.suby.project.login.dao.jpa.UserRoleJpaRepository;
import net.suby.project.login.vo.UserAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.security.SocialUser;
import org.springframework.stereotype.Service;

/**
 * Created by ohs on 2017-01-12.
 */
@Service("customUserDetailsService")
public class CustomUserDetailsService implements UserDetailsService {

    @Autowired
    private UserJpaRepository userJpaRepository;

    @Autowired
    private UserRoleJpaRepository userRoleJpaRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserAccounts user = userJpaRepository.findByEmail(username);
        List<String> userRoles = userRoleJpaRepository.findRoleByUserName(username);
        List<GrantedAuthority> authorities = buildUserAuthority(userRoles);

        return buildUserForAuthentication(user, authorities);
    }

    private SocialUser buildUserForAuthentication(UserAccounts user, List<GrantedAuthority> authorities) {
        return new SocialUser(user.getEmail(), user.getPassword(), true, true, true, true, authorities);
    }

    // 권한 설정
    private List<GrantedAuthority> buildUserAuthority(List<String> userRoles) {

        Set<GrantedAuthority> setAuths = new HashSet<>();

        // Build user's authorities
        for (String userRole : userRoles) {
            setAuths.add(new SimpleGrantedAuthority(userRole));
        }

        List<GrantedAuthority> Result = new ArrayList<>(setAuths);

        return Result;
    }

}
