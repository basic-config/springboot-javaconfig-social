package net.suby.project.config.security;

import javax.sql.DataSource;
import net.suby.project.config.persistence.PersistenceJpaConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.social.security.SpringSocialConfigurer;

@Configuration
@EnableWebSecurity
@ComponentScan(basePackageClasses = {CustomUserDetailsService.class, PersistenceJpaConfig.class})
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("customUserDetailsService")
    private UserDetailsService userDetailsService;

    @Autowired
    @Qualifier("jpaDataSource")
    private DataSource dataSource;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService);	//.passwordEncoder(passwordEncoder());
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http
            .formLogin()
                .loginPage("/login").failureUrl("/login?error")
                .defaultSuccessUrl("/")
                .usernameParameter("username").passwordParameter("password")
                .permitAll()
//          .and()
//              .rememberMe()
//                // persistent방식(S) - DB에 저장이 필요하기 때문에 persistent_logins테이블이 필요 - 쿠키방식보다 안전
//                .tokenRepository(persistentTokenRepository())
//                .tokenValiditySeconds(1209600)
                // persistent방식(S)

                // 쿠키방식(S)
//				.key("rem-me-key")
//				.rememberMeParameter("remember-me")
//				.rememberMeCookieName("my-remember-me")
//				.tokenValiditySeconds(86400)
                // 쿠키방식(E)
            .and()
                .logout()
                    .logoutUrl("/logout")
                    .deleteCookies("JSESSIONID")
                    .logoutSuccessUrl("/login?logout")
            .and()
                .authorizeRequests()
                    .antMatchers(
                        "/auth/**",
                        "/login",
                        "/signup/**",
                        "/signin/**",
                        "/register/**"
                    ).permitAll()
                    .antMatchers("/admin/**").access("hasRole('ROLE_ADMIN')")
                    .antMatchers("/user/**").access("hasRole('ROLE_USER')")
                    .antMatchers("/**").access("hasRole('ROLE_USER')")
            .and()
                .apply(new SpringSocialConfigurer())    // 소셜 로그인 설정
            .and()
                .csrf();		// 크로스도메인 체크를 위해서 모든 form에 token 정보를 넣어줘야 한다.
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
        db.setDataSource(dataSource);
        return db;
    }

    @Bean
    public PasswordEncoder passwordEncoder(){
        PasswordEncoder encoder = new BCryptPasswordEncoder();
        return encoder;
    }

}