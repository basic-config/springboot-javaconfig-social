package net.suby.project.login.service.impl;

import net.suby.project.login.dao.jpa.UserJpaRepository;
import net.suby.project.login.service.LoginService;
import net.suby.project.login.vo.UserAccounts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private UserJpaRepository userJpaRepository;

    @Override
    public UserAccounts findByEmail(String email) {
        return userJpaRepository.findByEmail(email);
    }
}