package net.suby.project.login.service;

import net.suby.project.login.vo.UserAccounts;

/**
 * Created by ohs on 2017-01-12.
 */
public interface LoginService {
    public UserAccounts findByEmail(String email);
}
